#![allow(unused)] // Only during early development work.

use core::panic;

use gfx_deduplicator::{value::{Palette, Palettes, Tile, TileMap8x8, Color}, optimizer::{ReplacementMap, ReplacementOperation}};
use itertools::izip;

fn main() {
    // dbg_palettes();
    // dbg_tile();
    // dbg_parse_tiles();
    // dbg_copy_optimized_tileset();
    // dbg_rebuild_original_from_optimized_map();
    dbg_tile_map_to_bytes()
}

#[allow(unused)]
fn dbg_palettes() {
    let default_palette = Palette::default();
    println!(
        "Default palette:\n{}\n{} Default",
        default_palette.format_color_rows(),
        default_palette.format_single_row(),
    );

    // let palette_105_bytes = std::fs::read("./examples/105.pal").unwrap();
    let palettes_105 = Palettes::from_pal_file("./examples/105.pal").unwrap();

    let index_display_length = (palettes_105.len() as f32).log(16f32).ceil() as usize;
    for (i, palette) in palettes_105.iter().enumerate() {
        println!("{} (Index {i:index_display_length$X})", palette.format_single_row());
    }
}

#[allow(unused)]
fn dbg_tile() {
    let palette = Palette::default();

    dbg_print_tile(&Tile::new_debug_tile_gem(), &palette);

    let tile = Tile::new_debug_tile_corner();

    println!("The tile as-is");
    dbg_print_tile(&tile, &palette);
    println!();

    println!("The tile with flipped variants");
    dbg_print_two_tiles(&tile, &tile.flipped_x(), &palette);
    dbg_print_two_tiles(&tile.flipped_y(), &tile.flipped_xy(), &palette);

    // dbg_print_tile(&tile.mirrored_y(), &palette);
    // println!();

    // dbg_print_tile(&tile.mirrored_x(), &palette);
    // println!();

    // dbg_print_tile(&tile.mirrored_xy(), &palette);
    // println!();
}

fn dbg_print_tile_indices(tile: &Tile) {
    for line in tile.lines() {
        for value in line {
            print!("{value:X} ");
        }
        println!();
    }
}


fn dbg_print_tile(tile: &Tile, palette: &Palette) {
    for line in tile.lines() {
        print_tile_line(&line, palette);
        println!();
    }
}

fn dbg_print_tile_sm(tile: &Tile, palette: &Palette) {
    for chunk in tile.lines().chunks(2) {
        print_tile_line_sm(&chunk[0], &chunk[1], palette);
    }
}

fn dbg_print_two_tiles(tile_a: &Tile, tile_b: &Tile, palette: &Palette) {
    // This does not need to be an izip!, but it's needed later when printing more than two tiles
    for (line_a, line_b) in izip!(&tile_a.lines(), &tile_b.lines()) {
        print_tile_line(line_a, palette);
        print_tile_line(line_b, palette);

        println!();
    }
}

fn dbg_print_16_tiles(tiles: &[Tile], palette: &Palette) {
    let empty_tile = Tile::new_empty();

    // This does not need to be an izip!, but it's needed later when printing more than two tiles.
    // The row might not contain exactly 16 tiles (because optimizing might remove some), so we need empty tiles to fill.
    let zip = izip!(
        tiles[0].lines(),
        tiles.get(1).unwrap_or(&empty_tile).lines(),
        tiles.get(2).unwrap_or(&empty_tile).lines(),
        tiles.get(3).unwrap_or(&empty_tile).lines(),
        tiles.get(4).unwrap_or(&empty_tile).lines(),
        tiles.get(5).unwrap_or(&empty_tile).lines(),
        tiles.get(6).unwrap_or(&empty_tile).lines(),
        tiles.get(7).unwrap_or(&empty_tile).lines(),
        tiles.get(8).unwrap_or(&empty_tile).lines(),
        tiles.get(9).unwrap_or(&empty_tile).lines(),
        tiles.get(10).unwrap_or(&empty_tile).lines(),
        tiles.get(11).unwrap_or(&empty_tile).lines(),
        tiles.get(12).unwrap_or(&empty_tile).lines(),
        tiles.get(13).unwrap_or(&empty_tile).lines(),
        tiles.get(14).unwrap_or(&empty_tile).lines(),
        tiles.get(15).unwrap_or(&empty_tile).lines(),
    );

    // zip does not support iterating in chunks, which is needed to draw two lines simultaneously, so we collect the lines first
    let mut lines: Vec<Vec<u8>> = Vec::new();
    for tile in zip {
        let mut line: Vec<u8> = Vec::with_capacity(16 * 8);
        line.extend_from_slice(tile.0.as_slice());
        line.extend_from_slice(tile.1.as_slice());
        line.extend_from_slice(tile.2.as_slice());
        line.extend_from_slice(tile.3.as_slice());
        line.extend_from_slice(tile.4.as_slice());
        line.extend_from_slice(tile.5.as_slice());
        line.extend_from_slice(tile.6.as_slice());
        line.extend_from_slice(tile.7.as_slice());
        line.extend_from_slice(tile.8.as_slice());
        line.extend_from_slice(tile.9.as_slice());
        line.extend_from_slice(tile.10.as_slice());
        line.extend_from_slice(tile.11.as_slice());
        line.extend_from_slice(tile.12.as_slice());
        line.extend_from_slice(tile.13.as_slice());
        line.extend_from_slice(tile.14.as_slice());
        line.extend_from_slice(tile.15.as_slice());

        lines.push(line);
    }

    lines.chunks(2).for_each(|chunk| {
        print_tile_line_sm(&chunk[0], &chunk[1], palette);
    })

    // Print it big
    // for tile in zip {
        // print_tile_line(&tile.0, palette);
        // print_tile_line(&tile.1, palette);
        // print_tile_line(&tile.2, palette);
        // print_tile_line(&tile.3, palette);
        // print_tile_line(&tile.4, palette);
        // print_tile_line(&tile.5, palette);
        // print_tile_line(&tile.6, palette);
        // print_tile_line(&tile.7, palette);
        // print_tile_line(&tile.8, palette);
        // print_tile_line(&tile.9, palette);
        // print_tile_line(&tile.10, palette);
        // print_tile_line(&tile.11, palette);
        // print_tile_line(&tile.12, palette);
        // print_tile_line(&tile.13, palette);
        // print_tile_line(&tile.14, palette);
        // print_tile_line(&tile.15, palette);

    //     println!("\x1B[0m|");
    // }

}

fn print_tile_line(line: &Vec<u8>, palette: &Palette) {
    for index in line {
        if *index == 0 {
            // print!("░░");
            print!("  ");
            continue;
        }

        let color = palette.get_color_unchecked(*index);
        print!(
            "\x1B[48;2;{};{};{}m  \x1B[0m",
            color.red,
            color.green,
            color.blue,
        );
    }
}

fn print_tile_line_sm(line: &Vec<u8>, next_line: &Vec<u8>, palette: &Palette) {
    if line.len() != next_line.len() {
        panic!("Both lines must be of same length (8)");
    }

    let mut clear_requires_reset = false;

    for i in 0..line.len() {
        let index_top = line[i];
        let index_bottom = next_line[i];

        if index_top == 0 && index_bottom == 0 {
            // print!("░░");
            if clear_requires_reset {
                print!("\x1b[0m ");
            } else {
                print!(" ");
                clear_requires_reset = false;
            }
            continue;
        }

        clear_requires_reset = true;

        if index_top == index_bottom {
            let color = palette.get_color_unchecked(index_top);
            print!(
                "\x1B[38;2;{};{};{}m█",
                color.red,
                color.green,
                color.blue,
            );
            continue;
        }

        if index_top == 0 {
            let color = palette.get_color_unchecked(index_bottom);
            print!(
                "\x1B[0;38;2;{};{};{}m▄",
                color.red,
                color.green,
                color.blue,
            );
            continue;
        }

        if index_bottom == 0 {
            let color = palette.get_color_unchecked(index_top);
            print!(
                "\x1B[0;38;2;{};{};{}m▀",
                color.red,
                color.green,
                color.blue,
            );
            continue;
        }

        let color_top = palette.get_color_unchecked(index_top);
        let color_bottom = palette.get_color_unchecked(index_bottom);

        print!(
            "\x1B[38;2;{};{};{};48;2;{};{};{}m▀",
            color_top.red,
            color_top.green,
            color_top.blue,
            color_bottom.red,
            color_bottom.green,
            color_bottom.blue,
        );
    }

    println!("\x1B[0m");
}

#[allow(unused)]
fn dbg_parse_tiles() {
    let file = std::fs::read("./examples/Template-Unoptimized.gfx").unwrap();
    let tile_map = TileMap8x8::parse_from_bytes(file).unwrap();

    let palette = Palette::default();
    tile_map.tiles().chunks(16).for_each(|tile_row: &[Tile]| {
        dbg_print_16_tiles(tile_row, &palette);
    });
}

fn dbg_copy_optimized_tileset() {
    let file = std::fs::read("./examples/Template-Unoptimized.gfx").unwrap();
    // let file = std::fs::read("./examples/GFX00.bin").unwrap();
    let tile_map = TileMap8x8::parse_from_bytes(file).unwrap();

    // let tiles = TileMap8x8::new(vec![Tile::new_debug_tile_gem()]);
    // println!("{}", tiles.contains_exact(&Tile::new_debug_tile_gem()));

    let optimized = tile_map.optimize();

    println!("{:#?}\n", optimized.borrow_deduplicated_summary());

    // TODO: Refactor this away in its own module. this is what the main tool should display.
    println!("Minimap of optimization operations (██ = new | ██ = exact duplicate | ██ = flipped x | ██ = flipped y | ██ = flipped xy):");
    print_replacement_map(optimized.borrow_replacement_map());

    println!("\nDepiction of optimized tile map:");
    optimized.borrow_tile_map().tiles().chunks(16).for_each(|tile_row: &[Tile]| {
        dbg_print_16_tiles(tile_row, &Palette::default());
    });

    // TODO: Also try to create a MetaTileMap (Map16)
    // MetaTile::ACT_AS_GENERIC_SOLID
}

fn dbg_rebuild_original_from_optimized_map() {
    let file = std::fs::read("./examples/Template-Unoptimized.gfx").unwrap();
    let tile_map = TileMap8x8::parse_from_bytes(file).unwrap();

    let optimized_result = tile_map.optimize();

    println!("\nDepiction of optimized tile map:");
    optimized_result.borrow_tile_map().tiles().chunks(16).for_each(|tile_row: &[Tile]| {
        dbg_print_16_tiles(tile_row, &Palette::default());
    });

    let recreated = TileMap8x8::parse_from_replacement_map(
        optimized_result.borrow_replacement_map(),
        optimized_result.borrow_tile_map(),
    );

    println!("\nRecreated unoptimized tile map:");
    recreated.tiles().chunks(16).for_each(|tile_row: &[Tile]| {
        dbg_print_16_tiles(tile_row, &Palette::default());
    });
}

fn print_replacement_map(replacement_map: &ReplacementMap) {
    replacement_map.borrow_replacement_pointers().chunks(16).for_each(|row| {
        for pointer in row {
            let operation = pointer.borrow_operation();
            if let ReplacementOperation::Empty = operation {
                print!("\x1B[0;9m  \x1B[0m");
                continue;
            }

            let color = match operation {
                ReplacementOperation::Empty => unreachable!("handled above"),
                // TODO: Find good colors (and a sample gfx that has flipped duplicates)
                ReplacementOperation::New => Color::new(100, 125, 100),
                ReplacementOperation::Exact => Color::new(150, 150, 150),
                ReplacementOperation::FlippedX => Color::new(145, 125, 100),
                ReplacementOperation::FlippedY => Color::new(145, 125, 125),
                ReplacementOperation::FlippedXY => Color::new(145, 100, 125),
            };

            print!(
                "\x1B[48;2;{};{};{}m  ",
                color.red,
                color.green,
                color.blue,
            );
        }

        println!("\x1B[0m");
    })
}

fn dbg_tile_map_to_bytes() {
    let tile_map = TileMap8x8::new(vec![Tile::new_debug_tile_orb()]);

    let optimized_result = tile_map.optimize();
    let bytes = optimized_result.take_tile_map().convert_to_bytes();

    println!("{:0>2X?}", bytes);

    let re_parsed = TileMap8x8::parse_from_bytes(bytes).unwrap();
    dbg_print_tile_sm(re_parsed.get_tile_unchecked(0), &Palette::default());
}
