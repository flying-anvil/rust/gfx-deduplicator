#[derive(Debug)]
pub struct Color {
    pub red: u8,
    pub green: u8,
    pub blue: u8,
}

impl Color {
    pub fn new(red: u8, green: u8, blue: u8) -> Self { Self { red, green, blue } }
    pub fn new_uniform(value: u8) -> Self { Self { red: value, green: value, blue: value } }
    pub fn new_greyscale(brightness: f32) -> Self { Self::new_uniform((255.0 / brightness) as u8) }

    pub fn luminance(&self) -> f32 {
        (
            0.299 * self.red as f32 +
            0.587 * self.green as f32 +
            0.114 * self.blue as f32
        ) / 255.0
    }
}
