use std::path::Path;

use crate::{value::Color, prelude::{Result, Error}};

#[derive(Debug)]
pub struct Palette {
    colors: [Color; 16],
}

impl Default for Palette {
    fn default() -> Self {
        Self { colors: [
            Color::new_uniform(0),   // Index 0 ist transparent
            Color::new(0xE8, 0xF0, 0xF8), // Index 1 is almost white for all vanilla palettes
            Color::new_uniform(0),   // Index 2 is usually black (for outlines)

            // Grayscale from dark to bright
            Color::new_uniform(88),
            Color::new_uniform(120),
            Color::new_uniform(152),
            Color::new_uniform(192),
            Color::new_uniform(224),

            // Vanilla palettes usually don't use more than 8 colors for blocks. Let's use something more colorful
            Color::new(0x34, 0x05, 0x49),
            Color::new(0x4A, 0x1A, 0x5D),
            Color::new(0x66, 0x33, 0x78),
            Color::new(0x82, 0x4D, 0x91),
            Color::new(0x9E, 0x67, 0xAB),
            Color::new(0xB9, 0x81, 0xC5),
            Color::new(0xD5, 0x9A, 0xDF),
            Color::new(0xF2, 0xB4, 0xF8),
        ] }
    }
}

const SIZE_SINGLE_PALETTE: usize = 3 * 16;

impl Palette {
    pub fn new(colors: [Color; 16]) -> Self { Self { colors } }

    /// Loads a palette a .pal file (bytes).
    /// A .pal file contains RGB values for colors, stored as one byte per channel/three bytes per color.
    /// Such file typically contains multiple 16-color palettes, so the palette_index defines which one to get (0-based).
    ///
    /// This function assumes the format to be correct and that the palette_index actually exists (there are enough bytes)
    pub fn from_pal_bytes(bytes: &Vec<u8>, palette_index: u8) -> Self {
        let offset_lower = SIZE_SINGLE_PALETTE * (palette_index as usize);
        let offset_upper = offset_lower + SIZE_SINGLE_PALETTE;

        if bytes.len() < offset_upper {
            panic!(
                "Palette with index {} resides in bytes [{}, {}), but only {} bytes are given",
                palette_index,
                offset_lower,
                offset_upper,
                bytes.len(),
            );
        }

        // By the power of math we do know that we have exactly 16 colors (because we didn't panic)
        let mut colors: [Color; 16] = unsafe { std::mem::MaybeUninit::zeroed().assume_init() };

        bytes[offset_lower..offset_upper].chunks(3)
            .enumerate()
            .for_each(|(index, chunk)| colors[index] = Color::new(chunk[0], chunk[1], chunk[2]));

        Self::new(colors)
    }

    pub fn format_color_rows(&self) -> String {
        self.colors.iter().enumerate().map(|(index, color)| {
            if index == 0 {
                return format!(
                    " {:#04X} {:#04X} {:#04X} | #{:02X}{:02X}{:02X}   \x1B[2;3m(transparent)\x1B[0m",
                    color.red,
                    color.green,
                    color.blue,
                    color.red,
                    color.green,
                    color.blue,
                );
            }

            let background = format!(
                "48;2;{};{};{}",
                color.red,
                color.green,
                color.blue,
            );

            // Try to prevent hard-to-read colors
            let luminance = color.luminance();
            let foreground = if luminance > 0.325 && luminance < 0.825 {
                "38;2;0;0;0".to_string()
            } else {
                format!(
                    "38;2;{};{};{}",
                    u8::MAX - color.red,
                    u8::MAX - color.green,
                    u8::MAX - color.blue,
                )
            };

            format!(
                "\x1B[{};{}m {:#04X} {:#04X} {:#04X} | #{:02X}{:02X}{:02X} \x1B[0m",
                background,
                foreground,
                color.red,
                color.green,
                color.blue,
                color.red,
                color.green,
                color.blue,
            )
        }).collect::<Vec<String>>().join("\n")
    }

    pub fn format_single_row(&self) -> String {
        // TODO: Do the math and remove the runtime calculation
        let mut result = String::with_capacity("\x1B[38;2;123;123;123m  ".len() * 16 + "\x1B[0m".len());

        for color in &self.colors {
            result.push_str(&format!(
                "\x1B[48;2;{};{};{}m  \x1B[0m",
                color.red,
                color.green,
                color.blue,
            ))
        }

        result
    }

    pub fn get_color(&self, index: u8) -> Option<&Color> {
        self.colors.get(index as usize)
    }

    pub fn get_color_unchecked(&self, index: u8) -> &Color {
        unsafe { self.colors.get_unchecked(index as usize) }
    }
}

pub struct Palettes (Vec<Palette>);

impl Palettes {
    pub fn from_pal_file<P: AsRef<Path>>(path: P) -> Result<Self> {
        let bytes = std::fs::read(path).map_err(|err| Error::Generic(format!("Could not load file: {}", err)))?;

        let palettes = bytes.chunks(SIZE_SINGLE_PALETTE).filter_map(|chunk| {
            if chunk.len() != SIZE_SINGLE_PALETTE {
                return None;
            }

            Some(Palette::from_pal_bytes(&chunk.to_vec(), 0))
        }).collect::<Vec<Palette>>();

        Ok(Self(palettes))
    }

    pub fn get_palette(&self, palette_index: usize) -> Option<&Palette> {
        self.0.get(palette_index)
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn iter(&'_ self) -> impl Iterator<Item=&Palette> + '_ {
        self.0.iter()
    }
}
