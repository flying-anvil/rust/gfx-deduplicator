#![allow(unused)] // Only during early development work.

use crate::optimizer::ReplacementMap;

#[derive(Debug, Clone)]
pub struct MetaTileMap {
    virtual_width: u8,
    tiles: Vec<MetaTile>,
}

impl MetaTileMap {
    // This whole thing is supposed to be a "Map16".
    // SMW's Map16 has 16 MetaTiles per row, meaning 32 8x8 tiles.
    // The input we get is a gfx file, which is usually represented with a width of 16 8x8 tiles.
    // Thus the ReplacementMap also "has" 16 8x8 tiles per row.
    // Either we only support 8-wide Map16s, or ignore that and do some magic (maybe utilizing the virtual_width?).
    //
    // When iterating each ReplacementPointer, we only iterate a single MetaTile corner,
    // meaning we cannot create each MetaTiles at once on the fly.
    pub fn parse_from_replacement_map(replacement_map: &ReplacementMap, virtual_width: u8) -> Self {
        // let mut meta_tiles = Vec::with_capacity(replacement_map.len());
        let mut meta_tiles = vec![MetaTile::default(); replacement_map.len() / 4];

        let width_meta = virtual_width as usize;
        let width_tile = width_meta * 2;
        for (index, meta_tile) in meta_tiles.iter_mut().enumerate() {
            let (
                index_top_left,
                index_top_right,
                index_bottom_left,
                index_bottom_right,
            ) = meta_tile_index_to_tile_indices(index, width_meta);

            todo!("Set 8x8 tiles into the MetaTile");
        }

        // for (index, replacement_pointer) in replacement_map.borrow_replacement_pointers().iter().enumerate() {
        //     let meta_tile_index = tile_index_to_metatile_index(index);
        //     let mut meta_tile = unsafe {meta_tiles.get_unchecked(index)};
        // }

        // for pointer in replacement_map.borrow_replacement_pointers() {
        //     let mete_tile = match pointer.borrow_operation() {
        //         crate::optimizer::ReplacementOperation::Empty => MetaTile,
        //         crate::optimizer::ReplacementOperation::New => todo!(),
        //         crate::optimizer::ReplacementOperation::Exact => todo!(),
        //         crate::optimizer::ReplacementOperation::FlippedX => todo!(),
        //         crate::optimizer::ReplacementOperation::FlippedY => todo!(),
        //         crate::optimizer::ReplacementOperation::FlippedXY => todo!(),
        //     };
        // }

        Self {
            virtual_width: 16,
            tiles: meta_tiles,
        }
    }
}

#[derive(Debug, Default, Clone)]
pub struct MetaTile {
    corner_top_left: MetaTileCorner,
    corner_bottom_left: MetaTileCorner,
    corner_top_right: MetaTileCorner,
    corner_bottom_right: MetaTileCorner,
    /// This is an index to a behavior (0x0000-0x7FFF)
    act_as: u16,
}

impl MetaTile {
    /// This "Act as" is used by the vanilla cement block.
    pub const ACT_AS_GENERIC_SOLID: u16 = 0x130;
    pub const ACT_AS_NON_SOLID: u16 = 0x025;
}


// TODO: Find a better name.
/// Those are the 8x8 tiles that a MetaTile consists of.
#[derive(Debug, Default, Clone)]
pub struct MetaTileCorner {
    palette: u8,
    priority: bool, // Whether this tile is in front of sprites.
    /// This is an index to an 8x8 gfx (0x000-0x3FF)
    gfx: u16,
    flipped_x: bool,
    flipped_y: bool,
}

impl MetaTileCorner {
    /// In vanilla graphics, GFX 0x0F8 to 0x0FF are solid color tiles representing the
    /// first 8 colors of the first palette. Thus 0x0F8 is all color 0, which is transparent.
    pub const GFX_INVISIBLE: u16 = 0x0F8;
}

#[inline(always)]
fn meta_tile_index_to_tile_indices(index: usize, width: usize) -> (usize, usize, usize, usize) {
    let row = index / width; // Should floor automatically, because integers.

    let index_in_row = index % width;
    let index_top_left = (index_in_row * 2) + (row * width * 4);

    (
        index_top_left,
        index_top_left + 1,
        index_top_left + 6,
        index_top_left + 7,
    )
}

fn tile_index_to_metatile_index(index: usize, width: u16) -> usize {
    0
}

#[cfg(test)]
#[allow(unreachable_code)]
mod tests {
    use crate::value::meta_tile_map::{tile_index_to_metatile_index, meta_tile_index_to_tile_indices};

    #[test]
    fn test_meta_tile_index_to_tile_indices() {
        let width = 3; // 6;

        let indices_meta_tile_0 = meta_tile_index_to_tile_indices(0, width);
        assert_eq!(0, indices_meta_tile_0.0, "MetaTile 0 TL 0");
        assert_eq!(1, indices_meta_tile_0.1, "MetaTile 0 TR 1");
        assert_eq!(6, indices_meta_tile_0.2, "MetaTile 0 BL 6");
        assert_eq!(7, indices_meta_tile_0.3, "MetaTile 0 BR 7");

        let indices_meta_tile_1 = meta_tile_index_to_tile_indices(1, width);
        assert_eq!(2, indices_meta_tile_1.0);
        assert_eq!(3, indices_meta_tile_1.1);
        assert_eq!(8, indices_meta_tile_1.2);
        assert_eq!(9, indices_meta_tile_1.3);

        let indices_meta_tile_2 = meta_tile_index_to_tile_indices(2, width);
        assert_eq!(4, indices_meta_tile_2.0);
        assert_eq!(5, indices_meta_tile_2.1);
        assert_eq!(10, indices_meta_tile_2.2);
        assert_eq!(11, indices_meta_tile_2.3);

        let indices_meta_tile_3 = meta_tile_index_to_tile_indices(3, width);
        assert_eq!(12, indices_meta_tile_3.0);
        assert_eq!(13, indices_meta_tile_3.1);
        assert_eq!(18, indices_meta_tile_3.2);
        assert_eq!(19, indices_meta_tile_3.3);

        let indices_meta_tile_4 = meta_tile_index_to_tile_indices(4, width);
        assert_eq!(14, indices_meta_tile_4.0);
        assert_eq!(15, indices_meta_tile_4.1);
        assert_eq!(20, indices_meta_tile_4.2);
        assert_eq!(21, indices_meta_tile_4.3);

        let indices_meta_tile_5 = meta_tile_index_to_tile_indices(5, width);
        assert_eq!(16, indices_meta_tile_5.0);
        assert_eq!(17, indices_meta_tile_5.1);
        assert_eq!(22, indices_meta_tile_5.2);
        assert_eq!(23, indices_meta_tile_5.3);
    }

    #[test]
    fn test_tile_index_to_metatile_index() {
        // Maybe this can be done easier.
        return;

        let width = 3; // 6;

        assert_eq!(0, tile_index_to_metatile_index(0, width));
        assert_eq!(0, tile_index_to_metatile_index(1, width));
        assert_eq!(0, tile_index_to_metatile_index(6, width));
        assert_eq!(0, tile_index_to_metatile_index(7, width));

        assert_eq!(1, tile_index_to_metatile_index(2, width));
        assert_eq!(1, tile_index_to_metatile_index(3, width));
        assert_eq!(1, tile_index_to_metatile_index(8, width));
        assert_eq!(1, tile_index_to_metatile_index(9, width));

        assert_eq!(2, tile_index_to_metatile_index(4, width));
        assert_eq!(2, tile_index_to_metatile_index(5, width));
        assert_eq!(2, tile_index_to_metatile_index(10, width));
        assert_eq!(2, tile_index_to_metatile_index(11, width));

        assert_eq!(3, tile_index_to_metatile_index(12, width));
        assert_eq!(3, tile_index_to_metatile_index(13, width));
        assert_eq!(3, tile_index_to_metatile_index(18, width));
        assert_eq!(3, tile_index_to_metatile_index(19, width));

        assert_eq!(4, tile_index_to_metatile_index(14, width));
        assert_eq!(4, tile_index_to_metatile_index(15, width));
        assert_eq!(4, tile_index_to_metatile_index(20, width));
        assert_eq!(4, tile_index_to_metatile_index(21, width));

        assert_eq!(5, tile_index_to_metatile_index(16, width));
        assert_eq!(5, tile_index_to_metatile_index(17, width));
        assert_eq!(5, tile_index_to_metatile_index(22, width));
        assert_eq!(5, tile_index_to_metatile_index(23, width));
    }
}
