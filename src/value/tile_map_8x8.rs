/// Each 8x8 tile has 64 pixels, thus 64 "elements".
/// Each element is an index in the range of 0..16.
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Tile ([u8; 64]);

pub const BYTES_PER_TILE: usize = 32;

const EMPTY_TILE_DATA: [u8; 64] = [0u8; 64];

impl Tile {
    pub fn new_debug_tile_orb() -> Self {
        Self([
            0x0, 0x0, 0x8, 0x8, 0x8, 0x8, 0x0, 0x0,
            0x0, 0x8, 0xF, 0xF, 0xF, 0xF, 0x8, 0x0,
            0x8, 0xF, 0xD, 0xF, 0xF, 0xB, 0xF, 0x8,
            0x8, 0xF, 0xB, 0xD, 0xB, 0xB, 0xF, 0x8,
            0x8, 0xF, 0xD, 0xB, 0xB, 0xA, 0xF, 0x8,
            0x8, 0xF, 0xB, 0xB, 0xA, 0xB, 0xF, 0x8,
            0x0, 0x8, 0xF, 0xF, 0xF, 0xF, 0x8, 0x0,
            0x0, 0x0, 0x8, 0x8, 0x8, 0x8, 0x0, 0x0,
        ])
    }

    pub fn new_debug_tile_gem() -> Self {
        Self([
            0x0, 0x0, 0xA, 0xA, 0xA, 0x0, 0x0, 0x0,
            0x0, 0xA, 0xE, 0x9, 0xC, 0xA, 0x0, 0x0,
            0xA, 0xC, 0xB, 0x9, 0xC, 0xB, 0xA, 0x0,
            0xA, 0x9, 0x9, 0xB, 0x9, 0x9, 0xA, 0x0,
            0xA, 0xE, 0xC, 0x9, 0xE, 0xB, 0xA, 0x0,
            0x0, 0xA, 0xB, 0x9, 0xB, 0xA, 0x0, 0x0,
            0x0, 0x0, 0xA, 0xA, 0xA, 0x0, 0x0, 0x0,
            0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
        ])
    }

    pub fn new_debug_tile_corner() -> Self {
        Self([
            0x0, 0x0, 0x2, 0x2, 0x2, 0x2, 0x2, 0x2,
            0x0, 0x2, 0xF, 0xD, 0xD, 0x9, 0x9, 0x9,
            0x2, 0xF, 0xD, 0x9, 0x9, 0x9, 0x9, 0x9,
            0x2, 0xD, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9,
            0x2, 0xD, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9,
            0x2, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9, 0x0,
            0x2, 0x9, 0x9, 0x9, 0x9, 0x9, 0x0, 0x0,
            0x2, 0x9, 0x9, 0x9, 0x9, 0x0, 0x0, 0x3,
        ])
    }

    pub fn new(data: [u8; 64]) -> Self { Self(data) }
    pub fn new_empty() -> Self { Self(EMPTY_TILE_DATA) }

    pub fn flipped_y(&self) -> Self {
        let mut result = [0u8; 64];

        for (line_index, line) in self.lines().iter().rev().enumerate() {
            for (index_index, color_index) in line.iter().enumerate() {
                let index = 8 * line_index + index_index;

                result[index] = *color_index;
            }
        }

        Self(result)
    }

    pub fn flipped_x(&self) -> Self {
        let mut result = [0u8; 64];

        for (line_index, line) in self.lines().iter().enumerate() {
            for (index_index, color_index) in line.iter().rev().enumerate() {
                let index = 8 * line_index + index_index;

                result[index] = *color_index;
            }
        }

        Self(result)
    }

    pub fn flipped_xy(&self) -> Self {
        let mut result = [0u8; 64];

        for (line_index, line) in self.lines().iter().rev().enumerate() {
            for (index_index, color_index) in line.iter().rev().enumerate() {
                let index = 8 * line_index + index_index;

                result[index] = *color_index;
            }
        }

        Self(result)
    }

    /// Returns a vector of lines, where each line is a vector of incides/u8
    ///
    /// It's returning a vector mess because arrays are difficult to deal with dynamically
    pub fn lines(&self) -> Vec<Vec<u8>> {
        self.0.chunks(8)
            .map(|chunk| chunk.to_vec())
            .collect::<Vec<Vec<u8>>>()
    }

    pub fn is_empty(&self) -> bool {
        self.0 == EMPTY_TILE_DATA
    }
}

pub struct TileMap8x8 (Vec<Tile>);

impl TileMap8x8 {
    pub fn new_empty() -> Self { Self (Vec::new()) }
    pub fn new(tiles: Vec<Tile>) -> Self { Self (tiles) }

    pub fn tiles(&self) -> &Vec<Tile> {
        &self.0
    }

    pub fn push_tile(&mut self, tile: Tile) {
        self.0.push(tile);
    }

    pub fn get_tile(&self, index: usize) -> Option<&Tile> {
        self.0.get(index)
    }

    pub fn get_tile_unchecked(&self, index: usize) -> &Tile {
        unsafe { self.0.get_unchecked(index) }
    }

    pub fn contains_exact(&self, tile: &Tile) -> bool {
        self.0.contains(tile)
    }

    pub fn index_of(&self, tile: &Tile) -> Option<usize> {
        self.0.iter().position(|current| current == tile)
    }

    pub fn contains_flip(&self, tile: &Tile) -> bool {
        self.0.contains(tile) ||
        self.0.contains(&tile.flipped_x()) ||
        self.0.contains(&tile.flipped_y()) ||
        self.0.contains(&tile.flipped_xy())
    }
}
