#![allow(unused)] // Only during early development work.

use std::slice::Iter;

use crate::value::TileMap8x8;

#[derive(Debug, Clone)]
pub struct DeDuplicationSummary {
    tiles_before: u16,
    tiles_after: u16,
    removed_empty_tiles: u16,
    deduplicated_exact: u16,
    deduplicated_flipped_x: u16,
    deduplicated_flipped_y: u16,
    deduplicated_flipped_xy: u16,
}

#[derive(Debug, Clone)]
pub enum ReplacementOperation {
    Empty,
    New,
    Exact,
    FlippedX,
    FlippedY,
    FlippedXY,
}


#[derive(Debug, Clone)]
pub struct ReplacementPointer {
    new_location: u16,
    operation: ReplacementOperation,
}

impl ReplacementPointer {
    pub fn new(new_location: usize, operation: ReplacementOperation) -> Self { Self { new_location: new_location as u16, operation } }
    pub fn borrow_operation(&self) -> &ReplacementOperation {&self.operation}
    pub fn new_location(&self) -> u16 {self.new_location}
}

#[derive(Debug, Clone)]
pub struct ReplacementMap (Vec<ReplacementPointer>);

impl ReplacementMap {
    pub fn iter(&self) -> Iter<'_, ReplacementPointer> {self.0.iter()}
    pub fn borrow_replacement_pointers(&self) -> &Vec<ReplacementPointer> {&self.0}
    pub fn len(&self) -> usize {self.0.len()}
    pub fn is_empty(&self) -> bool {self.0.is_empty()}
}

pub struct OptimizationResult {
    tile_map: TileMap8x8,
    deduplicated_summary: DeDuplicationSummary,
    replacement_map: ReplacementMap,
}

impl OptimizationResult {
    pub fn borrow_tile_map(&self) -> &TileMap8x8 {&self.tile_map}

    /// This method consumes the result, which should be OK because the tile map is the thing that matters.
    pub fn take_tile_map(self) -> TileMap8x8 {self.tile_map}

    pub fn borrow_deduplicated_summary(&self) -> &DeDuplicationSummary {&self.deduplicated_summary}
    pub fn borrow_replacement_map(&self) -> &ReplacementMap {&self.replacement_map}
}

impl TileMap8x8 {
    pub fn optimize(&self) -> OptimizationResult {
        let mut optimized = Self::new_empty();

        // let optimized_tiles = self.tiles()
        //     .iter()
        //     .filter_map(|tile| {
        //         if optimized.contains_exact(tile) {
        //             return Some(tile.clone());
        //         } else {
        //             None
        //         }
        //     })
        //     .collect::<Vec<Tile>>();

        let mut empty_tiles = 0;
        let mut deduplicated_exact = 0;
        let mut deduplicated_flipped_x = 0;
        let mut deduplicated_flipped_y = 0;
        let mut deduplicated_flipped_xy = 0;

        let mut replacement_map = Vec::with_capacity(self.tiles().len());

        for tile in self.tiles() {
            // Don't include empty tiles in optimized set
            if tile.is_empty() {
                empty_tiles += 1;
                replacement_map.push(ReplacementPointer::new(0, ReplacementOperation::Empty));
                continue;
            }

            // The following can likely be optimized by storing the flipped variants with the new optimized map
            // to reduce the amount of copying.
            // However, it's plenty fast already because the data is pretty small, so idc for now.

            // Check each possible flip-state, include it if it's new, discard it if it's already present
            // If the tile already exists in the optimized map, don't insert it again.
            // Instead, record that the exact tile is already present at index X.
            if let Some(index) = optimized.index_of(tile) {
                deduplicated_exact += 1;
                replacement_map.push(ReplacementPointer::new(index, ReplacementOperation::Exact));
                continue;
            }

            if let Some(index) = optimized.index_of(&tile.flipped_x()) {
                deduplicated_flipped_x += 1;
                replacement_map.push(ReplacementPointer::new(index, ReplacementOperation::FlippedX));
                continue;
            }

            if let Some(index) = optimized.index_of(&tile.flipped_y()) {
                deduplicated_flipped_y += 1;
                replacement_map.push(ReplacementPointer::new(index, ReplacementOperation::FlippedY));
                continue;
            }

            if let Some(index) = optimized.index_of(&tile.flipped_xy()) {
                deduplicated_flipped_xy += 1;
                replacement_map.push(ReplacementPointer::new(index, ReplacementOperation::FlippedXY));
                continue;
            }

            // If the tile was not present in any flipped combination, insert it as a new tile and record it as that.
            // Push it to the replacement_map first, as it reads the length of optimized. Flipping the order causes an off-by-one error.
            replacement_map.push(ReplacementPointer::new(optimized.tiles().len(), ReplacementOperation::New));
            optimized.push_tile(tile.clone());
        }

        let tiles_before = self.tiles().len() as u16;
        let tiles_after = optimized.tiles().len() as u16;

        OptimizationResult {
            tile_map: optimized,
            deduplicated_summary: DeDuplicationSummary {
                tiles_before,
                tiles_after,
                removed_empty_tiles: empty_tiles,
                deduplicated_exact,
                deduplicated_flipped_x,
                deduplicated_flipped_y,
                deduplicated_flipped_xy,
            },
            replacement_map: ReplacementMap(replacement_map),
        }
    }
}
