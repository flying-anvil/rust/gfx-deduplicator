use crate::{value::{TileMap8x8, Tile, BYTES_PER_TILE}, prelude::{Result, Error}, optimizer::ReplacementMap};

impl TileMap8x8 {
    pub fn parse_from_bytes(input: Vec<u8>) -> Result<Self> {
        if input.len() % BYTES_PER_TILE != 0 {
            return Err(Error::Generic(format!("input has to be multiple of 32 bytes long, {} bytes given", input.len())));
        }

        let mut tiles = Vec::with_capacity(input.len() / BYTES_PER_TILE);
        input.chunks(32).for_each(|raw_tile| {
            // We know that each chunk is 32 bytes, so we don't need to check for the only possible error.
            tiles.push(Tile::parse_from_bytes(raw_tile).unwrap());
        });

        Ok(TileMap8x8::new(tiles))
    }

    /// I'm not sure if there's a need for this, but it was a nice way to ensure that things are correct.
    /// The ReplacementMap *must* belong to the given TileMap8x8!
    ///
    /// ## Panics
    ///
    /// When the ReplacementMap does not belong to the given TileMap8x8, tiles might be missing.
    pub fn parse_from_replacement_map(replacement_map: &ReplacementMap, optimized_tile_map: &TileMap8x8) -> Self {
        const TILE_NOT_FOUND: &str = "optimized tile map does not contain the referenced tile. The ReplacementMap *must* belong to the given TileMap8x8!";

        let pointers = replacement_map.borrow_replacement_pointers();
        let mut tiles = Vec::with_capacity(pointers.len());

        for pointer in pointers {
            let new_location = pointer.new_location() as usize;

            match pointer.borrow_operation() {
                crate::optimizer::ReplacementOperation::Empty => tiles.push(Tile::new_empty()),
                crate::optimizer::ReplacementOperation::New | crate::optimizer::ReplacementOperation::Exact => {
                    tiles.push(optimized_tile_map.get_tile(new_location).expect(TILE_NOT_FOUND).clone())
                },
                crate::optimizer::ReplacementOperation::FlippedX => tiles.push(optimized_tile_map.get_tile(new_location).expect(TILE_NOT_FOUND).flipped_x()),
                crate::optimizer::ReplacementOperation::FlippedY => tiles.push(optimized_tile_map.get_tile(new_location).expect(TILE_NOT_FOUND).flipped_y()),
                crate::optimizer::ReplacementOperation::FlippedXY => tiles.push(optimized_tile_map.get_tile(new_location).expect(TILE_NOT_FOUND).flipped_xy()),
            }
        }

        TileMap8x8::new(tiles)
    }

    pub fn convert_to_bytes(&self) -> Vec<u8> {
        let mut bytes = vec![0u8; self.tiles().len() * BYTES_PER_TILE];

        for (tile_index, tile) in self.tiles().iter().enumerate() {
            for (y, line) in tile.lines().iter().enumerate() {
                let mut bitplanes = [0u8; 4];
                for (i, bitplane) in bitplanes.iter_mut().enumerate() {
                    for (x, value) in line.iter().enumerate() {
                        let read_bit = value & (1 << i);
                        // The read bit might be this for the second bitplane: 0010. Now we need to move it to the x position.
                        // First, move that bit the the far-right, then left to the corresponding x-index
                        *bitplane |= (read_bit >> i) << (7 - x);
                    }
                }

                // println!("BP1: {:0>8b}", bitplanes[0]);
                // println!("BP2: {:0>8b}", bitplanes[1]);
                // println!("BP3: {:0>8b}", bitplanes[2]);
                // println!("BP4: {:0>8b}", bitplanes[3]);

                let offset = (tile_index * BYTES_PER_TILE) + (y * 2);
                bytes[offset]      = bitplanes[0];
                bytes[offset + 1]  = bitplanes[1];
                bytes[offset + 16] = bitplanes[2];
                bytes[offset + 17] = bitplanes[3];
            }
        }

        bytes
    }
}

impl Tile {
    pub fn parse_from_bytes(bytes: &[u8]) -> Result<Self> {
        if bytes.len() != BYTES_PER_TILE {
            return Err(Error::Generic(format!("input has to be multiple of 32 bytes long, {} bytes given", bytes.len())));
        }

        let mut tile_data: [u8; 64] = [0u8; 64];

        // The data for each line is stored in 4 bitplanes.
        // The first two are next to each other. The last two too, but offset 16 bytes.
        for y in 0..8 {
            let row_offset = y * 2;

            let bitplane_1 = bytes[row_offset];
            let bitplane_2 = bytes[row_offset + 1];
            let bitplane_3 = bytes[row_offset + 16];
            let bitplane_4 = bytes[row_offset + 17];

            for x in 0..8 {
                let mask = 1 << x;
                let bit_1 = (bitplane_1 & mask) >> x;
                let bit_2 = (bitplane_2 & mask) >> x;
                let bit_3 = (bitplane_3 & mask) >> x;
                let bit_4 = (bitplane_4 & mask) >> x;

                let combined = bit_1 | bit_2 << 1 | bit_3 << 2 | bit_4 << 3;
                tile_data[y * 8 + (7 - x)] = combined;
            }
        }

        Ok(Self::new(tile_data))
    }
}
